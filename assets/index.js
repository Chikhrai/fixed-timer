class Timer {
	constructor(tick, minutesEl, secondsEl, toggleEl, startEl, pauseEl, resetEl) {
		this.seconds = 58;
		this.minutes = 0;
		this.counting = false;
		this.tick = tick;
		this.minutesEl = minutesEl;
		this.secondsEl = secondsEl;
		toggleEl.addEventListener('click', this.toggleStart.bind(this));
		startEl.addEventListener('click', this.startTimer.bind(this));
		pauseEl.addEventListener('click', this.pauseTimer.bind(this));
		resetEl.addEventListener('click', this.resetTimer.bind(this));
	}

	startTimer() {
		console.log('Click start timer');
		if (!this.timerId) {
			this.timerId = setInterval(this.count.bind(this), this.tick);
			this.counting = true;
			console.log('Timer started', this.timerId);
		}
	}

	stopTimer() {
		clearInterval(this.timerId);
		this.counting = false;
		this.timerId = undefined;
	}

	count() {
		this.seconds++;
		if (this.seconds >= 60) {
			this.setMinutes(this.minutes + 1);
			this.seconds = 0;
		}
		this.renderSeconds();
		console.log(`Tick: ${this.minutes}:${this.seconds}. Timer: ${this.timerId}`);
	}

	pauseTimer() {
		this.stopTimer();
	}

	toggleStart() {
		if (!this.counting) {
			this.startTimer();
		} else {
			this.stopTimer();
		}
	}

	setMinutes(value) {
		this.minutes = value;
		this.renderMinutes();
	}

	resetTimer() {
		this.stopTimer();
		this.setMinutes(0);
		this.seconds = 0;
		this.renderSeconds();
	}

	renderMinutes() {
		this.minutesEl.textContent = this.minutes;
	}

	renderSeconds() {
		this.secondsEl.textContent = this.seconds;
	}
}

const minutes = document.getElementById('minutes');
const seconds = document.getElementById('seconds');
const superStartButton = document.getElementById('start');
const superPauseButton = document.getElementById('pause');
const toggleStartButton = document.getElementById('toggleStart');
const superResetButton = document.getElementById('reset');
const superTimer = new Timer(
	1000,
	minutes,
	seconds,
	toggleStartButton,
	superStartButton,
	superPauseButton,
	superResetButton
);


const superTimer2 = new Timer(
	1000,
	document.getElementById('minutes2'),
    document.getElementById('seconds2'),
    document.getElementById('toggleStart2'),
	document.getElementById('start2'),
	document.getElementById('pause2'),
	document.getElementById('reset2')
);